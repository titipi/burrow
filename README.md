# Burrow
**interdependent pad backup**

## Description
Burrow is a rudimentary script for archiving interdependent etherpad collections that might be distributed over multiple servers and etherpad installs. It is aimed at [padologists](https://march.international/constant-padology/) that don't have access to the server that hosts their etherpad files or that work across different groups with each their own etherpad installations. 

Pads listed on any pad specified in the command of burrow.sh will be saved as textfiles at a given daily interval (when using crontab), off-line or on-line. The script removes old pads with the same name if they exceed the limit of 5. 

Burrow offers padologists a simple back-up plan for when pads are down or when needing to work off-line. Burrow protects against the unwanted loss of expired pads and lessens the dependency on always being connected. It also goes against the tendency to centralize installations, and towards a non-sovereign infrastructure approach.


## Usage

1. Make a main pad with links to pads that will need to be archived.  
2. Organise the list in the pad:  
  * Add each pad link with `https://` in a new line
  * If you want to categorize the pads then add a category, with `#` in the beginning, on top of pads that belong in this category. Titles should not include spaces, add `_` instead as the exanple below. End the list with a `#`. Avoid pad urls that include `#`. 
  * If you don't want to archive certain pads you can add `non-archived` at the end of the line of the url. For example:

```
# Category_1
https://somepadlink non-archived
https://someotherpadlink

# Category_2
https://otherpadlink
https://otherotherpadlink non-archived
https://padlink
https://randompadlink

# Category_3
https://apadlink

#

```   

3. Open a terminal and enter the directory you want to save the script.

4. Git clone the folder in your computer through the terminal. Either with   
`$ gitclone git@gitlab.constantvzw.org:titipi/burrow.git`   
(this is supported if you have SSH access to the git)   
or  
`$ git clone https://gitlab.constantvzw.org/titipi/burrow.git`

5. Enter the downloaded directory   
`$ cd burrow`

6. Run  
`$ bash burrow.sh [LINK OF YOUR PAD WITH LISTS OF PADS]`  
or login to a server, save the script in a folder and set up a scheduled job with crontab:  
First make your script executable:  
`chmod +x /full/path/to/script/burrow.sh`  
Then, open the crontab file in an editor:  
`$ crontab -e`  
and add the following (add the full path to the burrow.sh script):    
`0 0 * * * /bin/bash /full/path/to/script/burrow.sh [LINK OF YOUR PAD WITH LISTS OF PADS] /full/path/to/script/log/log.txt 2> /full/path/to/script/log/err.txt`
This will run Burrow every day at midnight: for other frequencies see the crontab manual. This also saves logs. 

## Acknowledgment

Burrow is named after the tunnels that rodents and other small animals dig. These tunnels function at the same time as conduit, habitat, temporary refuge or simply as a byproduct of their movement through matter.

Bash scripting by Aggeliki Diakrousi, hello etherdump! Written in the context of [The Social Life of XG](https://chanse.org/solixg/).

## License

CC4r https://constantvzw.org/wefts/cc4r.en.html

