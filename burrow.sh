#!/bin/bash

string="/export/txt"

INDEX_PAD=$1$string

# Change the amount of days files should be kept (not implemented yet)
# AMOUNT_OF_SAVED_VERSIONS="5"

ARCHIVE_LOCATION="archive"

# Current directory location. Location should follow global path system? (not implemented yet)
LOCATION=$(dirname "$0")

# LAUNCH BURROW


# Download INDEX_PAD

cd $LOCATION

wget $INDEX_PAD

# Create an `archive` directory if it doesn't exist already
if [ -d "${ARCHIVE_LOCATION}" ]; then
  echo "${ARCHIVE_LOCATION} does exist."
else
  mkdir "${ARCHIVE_LOCATION}"
fi


# Create a `log` directory if it doesn't exist already
if [ -d "log" ]; then
  echo "log does exist."
else
  mkdir "log"
fi

# Create a `temp` that will be removed later
mkdir "${ARCHIVE_LOCATION}/temp"
echo "${ARCHIVE_LOCATION}/temp created"

# Rename INDEX_PAD into tmp-links-all.txt

mv txt tmp-links-all.txt

> categories-temp.txt
# create different files according to if categories exist or not
cat tmp-links-all.txt | while read line; do
    # for lines that don't include `https`
    if ! [[ "$line" =~ "https" ]]; then
        if grep -e "#" tmp-links-all.txt;then
          > categories-temp.txt
          # Make a textfile with the categories only
          grep -e "#" tmp-links-all.txt > categories-temp.txt
          # Remove lines that are not links or categories
          grep -e https -e "#" tmp-links-all.txt > tmp-links.txt
        else
          # Remove lines that are not links
          grep -e https tmp-links-all.txt > tmp-links.txt
        fi
    fi
done

# remove the links with pads that are tagged as non-archived
cat tmp-links.txt | while read line; do
        if [[ "$line" =~ "non-archived" ]]; then
           grep -v "non-archived" tmp-links.txt > tmp-links-less.txt
        fi
done



# Remove the `#` and create a new textfile with categories
# cat categories.txt | while read line; do echo ${line}>>tmp-links.txt; done
cut -c 3- categories-temp.txt > categories.txt
sed -i '/^[ \t]*$/d' categories.txt

# Make directory for each of the categories

# Make a temporary textfile `folders` to check what category directories already exist 
ls ${ARCHIVE_LOCATION} > ${ARCHIVE_LOCATION}/folders.txt


cat categories.txt | while read -r line

do  
    # Check if directories with categories exist already
    folder=$(grep "${line}" "${ARCHIVE_LOCATION}"/folders.txt)
    if [ -n "$folder" ]; then
      echo "${line} directory exists"
    else
      mkdir ${ARCHIVE_LOCATION}/${line}
      echo "${line} made"
    fi
    mkdir ${ARCHIVE_LOCATION}/temp/temp_${line}
done 


# Download all files in the right directory according to category

# For each category in the textfile categories.txt:
cat categories.txt | while read -r line

do 
  # If no category then add them in the same category
  if grep -Fxq "#" tmp-links-less.txt
  then
    # Find the pad list in the list that are in the same category
    same_category=$(sed -n '/# '${line}'/{:a;n;/#/b;p;ba}' tmp-links-less.txt)
    echo -ee r "\e[35mPads in category ${line}: $same_category\e[0m"
  else
    same_category="$(cat tmp-links-less.txt)"
    echo -e "\e[35mPads in category No_Category: $same_category\e[0m"
  fi


  # Make a textfile with the links of these pads
  if [ ! -f ${ARCHIVE_LOCATION}/${line}.txt ]
  then 
    echo -e "\e[35m${ARCHIVE_LOCATION}/${line}.txt does not exist\e[0m"
  else
    echo -e "\e[35m${ARCHIVE_LOCATION}/${line}.txt exists and it will be removed\e[0m"
    rm ${ARCHIVE_LOCATION}/${line}.txt
  fi

  echo $same_category > ${ARCHIVE_LOCATION}/${line}tmp.txt
  echo $(tr ' ' '\n' < ${ARCHIVE_LOCATION}/${line}tmp.txt > ${ARCHIVE_LOCATION}/${line}.txt)


  echo "This is the file with the pads from category ${line}: ${ARCHIVE_LOCATION}/${line}.txt"

  # Append /export/txt at the end of each link

  sed -e "s/$/\/export\/txt/" -i ${ARCHIVE_LOCATION}/${line}.txt

  cat ${ARCHIVE_LOCATION}/${line}.txt | while read link; do wget --content-disposition ${link} -P ${ARCHIVE_LOCATION}/temp/temp_${line}; done 


  # Make a txt file that includes all the filenames of the category directory

  ls ${ARCHIVE_LOCATION}/${line} > ${ARCHIVE_LOCATION}/filenames_${line}.txt

  # For every newly donwloaded file check if there is a file with the same name and same content in the archive
  for file in ${ARCHIVE_LOCATION}/temp/temp_${line}/*.txt 

  do
    # Get the name of the filename without the `.txt`
    name_no_txt="${file%????}"
    # Remove the path and get the name of the pad
    name="${name_no_txt##*/}"
    echo "Filename of the downloaded file is $name"
    # Check if there is another file in the archive with the same name
    result=$(grep "$name" "${ARCHIVE_LOCATION}"/filenames_${line}.txt)
    echo "The file(s) that includes the filename of the new file is $result"
    if [ -z "$result" ];then
      # If there is no file with the same name in the archive then move the file into the archive
      mv "$file" "${ARCHIVE_LOCATION}/${line}/${name%.txt}_-$(date +"%Y-%m-%d").txt"
      echo "file moved"
    else
      # Find the last modified/saved file with the same name
      lastfile=$(ls -t "${ARCHIVE_LOCATION}"/${line}/"$name"* | head -n1) 
      echo "this is "${ARCHIVE_LOCATION}/${line}/${file%.txt}_-$(date +"%Y-%m-%d").txt"mvthe last saved file $lastfile"
      echo "this is the new file $file"
      # Use command `diff` to check if there is any difference between old an new file, we assign the result of the command on a variable 
      difference=$(diff $file $lastfile)
      echo "diff is $difference"
      # If the variable `difference` is empty then the files are the same and the new one should be deleted
      if [ -z "$difference" ];then
        echo -e "\e[32m$file and $lastfile are the same and thus $file will be removed\e[0m"
        rm "$file"
        echo -e "\e[35m$file has been removed\e[0m"
        
        # Remove old files if there are more than 5 files that include the same name in the archive 
        number_of_files=$(ls "${ARCHIVE_LOCATION}"/${line}/"$name"* | wc -l)
        echo "number of files is $number_of_files"
        if [ "$number_of_files" -gt 5 ]
        then
             # There are more files than the limit
             # So we need to remove the older ones.
             ls -t "${ARCHIVE_LOCATION}"/${line}/"$name"* | tail -n +6 | xargs -d '\n' rm
             echo "older files with the name $name removed"
        fi
      else
        # If the variable `difference` includes something then the newly downloaded file can be archived
        # Add date to filename
        echo -e "\e[32m$file and $lastfile are different\e[0m"
        mv "$file" "${ARCHIVE_LOCATION}/${line}/${name%.txt}_-$(date +"%Y-%m-%d").txt"
        echo -e "\e[35mnew file saved in the archive\e[0m"
      fi
    fi
  done
# Remove temporary files and directories
rm "${ARCHIVE_LOCATION}/filenames_${line}.txt"
rm "${ARCHIVE_LOCATION}/${line}.txt"
rm "${ARCHIVE_LOCATION}/${line}tmp.txt"
done

rm tmp-links*
rm categories*
rm "${ARCHIVE_LOCATION}/folders.txt"
rm -r "${ARCHIVE_LOCATION}/temp"
echo "THE END"
